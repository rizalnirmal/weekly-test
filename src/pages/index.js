import React, { useEffect } from "react"
import { graphql, navigate } from "gatsby"

const BlogIndex = ({ data }) => {
  const posts = data.allMarkdownRemark.edges
  const latestPost = posts[0]
  useEffect(() => {
    navigate(latestPost.node.fields.slug)
  }, [])
  return null
}
export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
        }
      }
    }
  }
`
