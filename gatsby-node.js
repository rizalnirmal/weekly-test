const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
const fs = require("fs")
const dayjs = require("dayjs")
const exec = require("child_process").exec

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blog-post.js`)
  const result = await graphql(
    `
      {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
              }
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    throw result.errors
  }

  // Create blog posts pages.
  const posts = result.data.allMarkdownRemark.edges

  posts.forEach((post, index) => {
    const previous = index === posts.length - 1 ? null : posts[index + 1].node
    const next = index === 0 ? null : posts[index - 1].node

    createPage({
      path: post.node.fields.slug,
      component: blogPost,
      context: {
        slug: post.node.fields.slug,
        previous,
        next,
      },
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}

const initialStartDate = "2019-08-5"
const initialWeekStartDate = dayjs(initialStartDate)
  .startOf("week")
  .format()
const blogFolderPath = path.resolve(__dirname, "content/blog")
const deployFilePath = path.resolve(__dirname, "deploy/deploy.sh")
const mergeRequestFilePath = path.resolve(
  __dirname,
  "deploy/open-merge-request.sh"
)

exports.onCreateDevServer = ({ app }) => {
  app.get("/hello", function(req, res) {
    const files = fs.readdirSync(blogFolderPath)

    const { date } = req.query
    let currentDate = dayjs().format()
    if (date) {
      currentDate = dayjs(date).format()
    }
    const daysDiff = dayjs(currentDate).diff(dayjs(initialWeekStartDate), "day")
    const postWeekNumber = Math.floor(daysDiff / 7) + 1

    const weekFolderName = `week-${postWeekNumber}`

    if (files.indexOf(weekFolderName) === -1) {
      const currentWeekStartDate = dayjs(currentDate)
        .startOf("week")
        .format()
      const currentWeekEndDate = dayjs(currentDate)
        .endOf("week")
        .format()
      makeNewWeekFileAndFolder(
        weekFolderName,
        postWeekNumber,
        currentWeekStartDate,
        currentWeekEndDate
      )
    }
    console.log("write in exiting folder")
    exec(`sh ${deployFilePath}`)
    exec(`sh ${mergeRequestFilePath}`)

    res.send("hello")
  })
}

function makeNewWeekFileAndFolder(folderName, weekNumber, startDate, endDate) {
  fs.mkdirSync(`${blogFolderPath}/${folderName}`)

  const initialFileContent = `---
title: Week ${weekNumber}
date: "${startDate}"
endDate: "${endDate}"
description: "Articles for week ${weekNumber}"
---
  `

  fs.writeFileSync(
    `${blogFolderPath}/${folderName}/index.md`,
    initialFileContent
  )
}
